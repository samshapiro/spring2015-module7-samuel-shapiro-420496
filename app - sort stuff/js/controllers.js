'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);

phonecatControllers.controller('PhoneListCtrl', ['$scope', '$rootScope', 'Phone',
  function($scope, $rootScope, Phone) {
    $scope.phones = Phone.query();
    $scope.orderProp = 'age';
    
    $rootScope.gphone1 = $scope.phones[1];
    $rootScope.gphone2 = $scope.phones[2];
    
    $scope.compare = function() {
      alert($scope.phone1);
      alert($scope.phone2);
    $rootScope.gphone1 = Phone.get({phoneId:$scope.phone1});
    $rootScope.gphone2 = Phone.get({phoneId:$scope.phone2});
    window.location = "#/compare";
    }
  }]);

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
  function($scope, $routeParams, Phone) {
    $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
      $scope.mainImageUrl = phone.images[0];
    });

    $scope.setImage = function(imageUrl) {
      $scope.mainImageUrl = imageUrl;
    }
    
  }]);

phonecatControllers.controller('PhoneCompareCtrl', ['$scope', '$rootScope','Phone',
  function($scope, $rootScope, Phone) {
     
    }
    
]);

